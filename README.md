Сначала:

```bash
npm install
```
Одноразовая сборка:

```bash
npm start
```

Запуск живой сборки на локальном сервере:

```bash
npm run live
```